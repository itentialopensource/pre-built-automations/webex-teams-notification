
## 0.0.16 [05-25-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/webex-teams-notification!8

---

## 0.0.15 [06-21-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/webex-teams-notification!7

---

## 0.0.14 [01-05-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/webex-teams-notification!6

---

## 0.0.13 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/webex-teams-notification!5

---

## 0.0.12 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/webex-teams-notification!4

---

## 0.0.11 [05-17-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/webex-teams-notification!1

---

## 0.0.10 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/webex-teams-notification!1

---

## 0.0.9 [05-05-2021]

* patch/2021-02-27T14-51-03

See merge request itential/sales-engineer/selabprebuilts/webex-teams-notification!6

---

## 0.0.8 [03-23-2021]

* patch/2021-02-27T14-51-03

See merge request itential/sales-engineer/selabprebuilts/webex-teams-notification!6

---

## 0.0.7 [02-27-2021]

* patch/2021-02-27T14-51-03

See merge request itential/sales-engineer/selabprebuilts/webex-teams-notification!6

---

## 0.0.6 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/notification-webexteams!5

---

## 0.0.5 [02-25-2021]

* Update README.md, images/NotificationWebexTeamsAutoCatalogForm.png,...

See merge request itential/sales-engineer/selabdemos/notification-webexteams!4

---

## 0.0.4 [02-24-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/notification-webexteams!1

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/notification-webexteams!1

---

## 0.0.2 [01-29-2021]

* Bug fixes and performance improvements

See commit 2e0c574

---\n
